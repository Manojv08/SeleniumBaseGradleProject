package dailychallengeweek2;

import java.util.Scanner;

public class PrintAtoB {

	public static void main(String[] args) {
		// To print the range of numbers with a condition

		Scanner in = new Scanner(System.in);
		System.out.print("Enter the beginning number: ");
		int a = in.nextInt();
		System.out.print("Enter the ending number: ");
		int b = in.nextInt();
		for (int i = a; i <= b; i++) {
			if(i%3==0 && i%5==0)
			{
				System.out.print("FIZZBUZZ" + " ");
			}else if(i%3==0)
			{
				System.out.print("FIZZ" + " ");
			}else if(i%5==0)
			{
				System.out.print("BUZZ" + " ");
			}else
			{
				System.out.print(i + " ");
			}
		}
		in.close();
	}
}