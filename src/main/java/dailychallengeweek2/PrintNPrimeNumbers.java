package dailychallengeweek2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PrintNPrimeNumbers {

	public static void main(String[] args) {
		// To print prime numbers till n number

		Scanner get = new Scanner(System.in);
		System.out.print("Enter the number till which you need to print prime numbers: ");
		int a = get.nextInt();

		List<Integer> x = new ArrayList<Integer>();

		if(a<=1)
		{
			System.out.println("Enter number greater than One");
		}else {
			for(int j=2;j<=a;j++)
			{
				boolean flag = true;			
				for(int i=2;i<j;i++)
				{
					if(j%i==0)
					{
						flag = false;
						break;
					}
				}

				if(flag)
				{
					x.add(j);
				}
				get.close();
			}
			System.out.println(x);
		}
	}
}