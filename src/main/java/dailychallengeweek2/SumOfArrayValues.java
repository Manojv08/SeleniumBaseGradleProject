package dailychallengeweek2;

import java.util.Scanner;

public class SumOfArrayValues {

	public static void main(String[] args) {
		// To add all the values of an array and print them
		
		Scanner get = new Scanner(System.in);
		System.out.print("Enter the length of the array: ");
		int l = get.nextInt();
		int[] a = new int[l];
		
		System.out.print("Enter the input array values: ");		
		
		for (int i=0;i<l;i++)
		{
			a[i] = get.nextInt();
		}
		
		int sum = 0;
		for (int b : a) {
			sum = sum + b;
		}
		
		System.out.println("Sum of the input array values is: " + sum);
		
		get.close();

	}

}