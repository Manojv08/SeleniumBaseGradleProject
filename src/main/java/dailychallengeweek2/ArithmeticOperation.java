package dailychallengeweek2;

import java.util.Scanner;

public class ArithmeticOperation {

	public static void main(String[] args) {
		// To do all the Arithmetic operations based on the user input

		Scanner in = new Scanner(System.in);
		System.out.print("Enter the Arithmetic Operation you want to perform: ");
		String a = in.next();
		System.out.print("Enter the First number: ");
		int b = in.nextInt();
		System.out.print("Enter the Second number: ");
		int c = in.nextInt();
		int d = 0;
		
		if(a.equalsIgnoreCase("add"))
		{
			d=1;
		} else if (a.equalsIgnoreCase("subtract")) {
			d=2;
		}else if (a.equalsIgnoreCase("multiply")) {
			d=3;
		}else if (a.equalsIgnoreCase("divide")) {
			d=4;
		}

		switch (d) {
		case 1 : System.out.println("Addition of 2 numbers are: " + (b+c));
		break;
		case 2 : System.out.println("Subtraction of 2 numbers are: "+ (b-c));
		break;
		case 3 : System.out.println("Multiplication of 2 numbers are: "+ (b*c));
		break;
		case 4 : System.out.println("Division of 2 numbers are: "+ (b/c));
		break;
		default: System.out.println("Enter valid operation");
		break;
		}
		in.close();

	}

}