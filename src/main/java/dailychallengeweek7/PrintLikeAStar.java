package dailychallengeweek7;

import java.util.Scanner;

public class PrintLikeAStar {

	public static void main(String[] args) {
		// To Print like a star
		
		int n, i, j, space = 1;
        System.out.print("Enter the number of rows: ");
        Scanner s = new Scanner(System.in);
        n = s.nextInt();
        space = n/2;
        for (j = 1; j <= (n/2)+1; j++) 
        {
            for (i = 1; i <= space; i++) 
            {
                System.out.print(" ");
            }
            space--;
            for (i = 1; i <= 2 * j - 1; i++) 
            {
                System.out.print(i);                
            }
            System.out.println("");
        }
        space = 1;
        int remainingrows = n/2;
        for (j = 1; j <= remainingrows; j++) 
        {
            for (i = 1; i <= space; i++) 
            {
                System.out.print(" ");
            }
            space++;
            for (i = 1; i <= ((remainingrows-j)*2)+1; i++) 
            {
                System.out.print(i);
            }
            System.out.println("");
        }
        s.close();
	}

}