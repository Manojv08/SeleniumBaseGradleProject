package week3Day2;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTable {

	public static void main(String[] args) {
		// To work with web tables
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://erail.in/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC",Keys.TAB);
		boolean selected = driver.findElementById("chkSelectDateOnly").isSelected();
		{
			if(selected) {
				driver.findElementById("chkSelectDateOnly").click();
			}
		}
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
	    List<WebElement> allrows = table.findElements(By.tagName("tr"));
	    System.out.println(allrows.size());
	    for (WebElement eachrow : allrows) {
			List<WebElement> allcols = eachrow.findElements(By.tagName("td"));
			System.out.println(allcols.get(1).getText());
		}
	}
}