package week3Day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class DteYbr {

	public static void main(String[] args) throws InterruptedException {
		// To Automate DTE UPoint site
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.get("http://digital.qc.alight.com/dteenergy/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.findElementById("_ParticipantLogon20_WAR_ahcommonauthportlet_userId").sendKeys("a18200087");
		driver.findElementById("_ParticipantLogon20_WAR_ahcommonauthportlet_password").sendKeys("A999999a");
		driver.findElementById("_ParticipantLogon20_WAR_ahcommonauthportlet_testCfgList[0].cfgValue").sendKeys("GJ4B");
		driver.findElementById("_ParticipantLogon20_WAR_ahcommonauthportlet_testCfgList[1].cfgValue").sendKeys("M6HB");
		boolean selected = driver.findElementById("_ParticipantLogon20_WAR_ahcommonauthportlet_skipAACheckbox").isSelected();
		if(!selected)
		{
			driver.findElementById("_ParticipantLogon20_WAR_ahcommonauthportlet_skipAACheckbox").click();
		}
		
		driver.findElementById("_ParticipantLogon20_WAR_ahcommonauthportlet_logOn").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//div[@id=\"flyout\"]/div[2]/div[1]/a").click();
	}

}