package week3Day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class webtabletest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		WebElement table = driver.findElementById("tablelocator");
		List<WebElement> allrows = table.findElements(By.tagName("tr"));
		int row = 1;
		for (WebElement eachrow : allrows) {
			List<WebElement> allcols = eachrow.findElements(By.tagName("td"));
			int col = 1; 
			for (WebElement eachcol : allcols) {
				String testhexa = eachcol.getText();
				if(testhexa.equals("hexa"))
				{
					System.out.println(row + "&" + col);
				}
				col++;
			}
			row++;
		}
	}
}