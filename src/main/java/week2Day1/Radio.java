package week2Day1;

public interface Radio {

	public void setFrequency();
	
	public void switchOn();
	
}