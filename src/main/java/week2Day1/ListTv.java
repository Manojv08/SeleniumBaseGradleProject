package week2Day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListTv {

	public static void main(String[] args) {
		// To find out all the Tv in the shop
		
		List<String> obj = new  ArrayList<String>();
		 
        obj.add("Samsung");
        obj.add("Onida");
        obj.add("BPL");
        obj.add("LG");
        obj.add("Onida");
        obj.add("Samsung");
        obj.add("Sony");
        System.out.println("Count of Tv's : " +obj.size());
        /*for (String eachTv : obj) {
        	
        	System.out.println(eachTv);
        	
        }*/
        
        System.out.println(obj);
        
        int a = obj.size();
        obj.remove(a-1);
        System.out.println("Removed Last tv" + "\n" + "Count of Tv's : " +obj.size());

        Collections.sort(obj);
        /*for (String orderTv : obj) {
        	System.out.println(orderTv);
			
		}*/
        
        System.out.println(obj);

	}

}