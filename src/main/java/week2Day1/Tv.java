package week2Day1;

public interface Tv {
	
	String defaultSetting = "Yes";
	
	public void switchOn();
	
	public void switchOff();
	
	public void switchChannel(int channelNumber);
	
	public void switchChannel(String channelName);

}