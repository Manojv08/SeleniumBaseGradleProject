package week2Day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LastCharInAscii {

	public static void main(String[] args) {
		// To find out the last character in the alphabetical order of the given input string
		
		String a = "i learnt today a lot";
		
		char[] b = a.toCharArray();
		
		List<Character> obj = new ArrayList<Character>();
		
		for (int i = 0; i < b.length; i++) {
			obj.add(b[i]);
		}
		
		Collections.sort(obj);
		
		int c = obj.size();
		
		System.out.println("The last character in the given input is: " + obj.get(c-1)); 
		
	}

}