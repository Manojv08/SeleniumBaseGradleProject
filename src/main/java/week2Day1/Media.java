package week2Day1;

public class Media implements SmartTv{

	public void switchOn() {
		// To switch on Television
		System.out.println("Your Tv is switched on now successfully");
		
	}

	public void switchOff() {
		// To switch Off Television
		System.out.println("Your Tv has been switched Off now");
		
	}
	
	public void switchChannel(int channelNumber) {
		// To change channels by number
		System.out.println("Selected Channel number "+ channelNumber + " is showing now");
	}

	public void switchChannel(String channelName) {
		// To change channels by name
		System.out.println("Selected Channel name "+ channelName + " is showing now");
		
	}

	public void bluetooth() {
		// To connect bluetooth
		
		System.out.println("Bluetooth has been turned on and connected");
		
	}

	public void USBPort() {
		// To connect USB device
		
		System.out.println("Your USB device is connected");
		
	}

	public void setFrequency() {
		// TODO Auto-generated method stub
		
	}
	
}