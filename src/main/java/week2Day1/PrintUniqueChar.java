package week2Day1;

import java.util.LinkedHashSet;
import java.util.Set;

public class PrintUniqueChar {

	public static void main(String[] args) {
		// To print only the unique characters in the given string

		String a = "Cognizant India";

		char[] b = a.toCharArray();

		Set<Character> obj = new LinkedHashSet<Character>();

		for (int i = 0; i < b.length; i++) {
			obj.add(b[i]);
		}
		/*for (Character eachchar : obj) {
			System.out.println(eachchar);
		}*/
		System.out.println(obj);
	}

}