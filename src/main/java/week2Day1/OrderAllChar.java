package week2Day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OrderAllChar {

	public static void main(String[] args) {
		// To print all the given input characters in alphabetical order

		String a = "amazon development center";

		char[] b = a.toCharArray();

		List<Character> obj = new ArrayList<Character>();

		for (int i = 0; i < b.length; i++) {
			obj.add(b[i]);
		}
		
		Collections.sort(obj);

		for (Character c : obj) {
			System.out.println(c);
		}

	}
}