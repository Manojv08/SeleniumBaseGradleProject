package week1;

public class LearningObjects {

	/*public static void main(String[] args) {
		// Calling methods from MobilePhone class
		
		MobilePhone.Lock();
		MobilePhone.Unlock();
		MobilePhone.AddContact("Mano", 9095023454l);
		MobilePhone.CallPerson("Xyz");
		
		MobilePhone.AddContact("Sruthi", 9789548369l);
		MobilePhone.CallPerson("Sruthi");

	}*/
	
	public static void main(String[] args) {
		//Calling methods from BankAccount class
		
		BankAccount get = new BankAccount();
		get.createAccount("Savings", "Mano", 'V');
		
		
		BankAccount.printAccountDetails();
		
		boolean result = get.blockAccount(654321);
		System.out.println("Final status of account: " + result);
		
	}

}