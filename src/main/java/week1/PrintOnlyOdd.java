package week1;

import java.util.Scanner;

public class PrintOnlyOdd {

	public static void main(String[] args) {
		// To print only odd numbers in the input
		
		Scanner get = new Scanner(System.in);
		System.out.print("Enter the number: ");
		String a = get.next();
		int b = a.length();
		for(int i=0; i<b;i++)
		{
			if(a.charAt(i)%2!=0)
			{
				System.out.println(a.charAt(i));
			}
				
		}
		get.close();
	}

}