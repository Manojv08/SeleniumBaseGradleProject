package week1;

import java.util.Scanner;

public class MobilePhone {

	static long IMEINumber = 85275364567575L;
	String Make = "Apple";
	float price;
	int CameraMegaPixel;
	boolean isDualSimEnabled;
	char Multitasking;
	
	static Scanner get = new Scanner(System.in);

	public static void Lock() {
		System.out.println("Your phone with this IMEI Number: " + IMEINumber + " is locked now");
	}

	public static void Unlock() {
		System.out.println("Your phone is unlocked now");
	}

	public static void AddContact(String Name, long PhoneNumber) {
		System.out.println(Name + " and " + PhoneNumber + " has been added to your contacts now");
	}

	public static void CallPerson(String Name) {
		System.out.println("Calling " + Name);
	}

	static void Sendsms(long mobilenumber,String message) {
		System.out.println("This Message you have provided has been sent successfully to this number: " + mobilenumber);		
	}

	static long SearchContact(String Name) {
		System.out.println("Searching the Contact: " + Name);
		return 9095023454l;
	}

	static void EditContact(String Name) {
		System.out.println("Editing the contact: " + Name);
		System.out.print("Enter new number: ");
		long newnumber = get.nextLong();
		System.out.println("New updated number is: " + newnumber);
		}

	static void DeleteContact(String Name) {
		System.out.println("Deleted the following contact successfully: " + Name);
	}


	public static void main(String[] args) {
		// Mobile phone operations

		Lock();
		Unlock();
		AddContact("Manoj" , 9095023454L);
		CallPerson("Sruthi");
		Sendsms(9789548369L,"Hi");
		long mobnumber = SearchContact("Mano");
		System.out.println(mobnumber);
		EditContact("Angel");
		DeleteContact("Xyz");
	}

}