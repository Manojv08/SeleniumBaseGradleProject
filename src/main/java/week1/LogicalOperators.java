package week1;

import java.util.Scanner;

public class LogicalOperators {

	public static void main(String[] args) {
		// To use logical operators and play with

		Scanner get = new Scanner(System.in);
		System.out.print("Enter a: ");
		int a = get.nextInt();
		System.out.print("Enter b: ");
		int b = get.nextInt();
		
		while(a<5 || b<20)
		{
			System.out.println("Condition passed : Loop exceuted: "+ a);
			a++;
			b++;
		}
		get.close();
	}

}