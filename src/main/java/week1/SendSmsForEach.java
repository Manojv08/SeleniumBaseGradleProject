package week1;

public class SendSmsForEach {

	public static void main(String[] args) {
		// Using For each loop
		
		long a[] = {9000812341l,9000812342l,9000812343l,9000812344l,9000812345l,9000812346l};
		
		for(long num : a)
		{
			MobilePhone.Sendsms(num,"Good Morning! Happy Sunday!");
		}

	}

}