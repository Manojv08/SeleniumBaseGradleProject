package week1;

public class PrimitiveDataTypes {

	public static void main(String[] args) {
		// To print all types of primitive data types
		/* Range of all primitive data types n-number bits
		 * Range -2^n-1 to +2^n-1 - 1 till Java SE7
		 * Range -2^n to +2^n - 1 from Java SE8
		 */
		
		boolean flag = true;
		char a = 'S';
		byte b = 127;
		short c = 32767;
		int d = 123456789;
		long e = 12345678910l;
		float f = 987654.321f;
		double g = 9874535.245;
		String h = "Printing String value";
		
		System.out.println("Print boolean value: " + flag);
		System.out.println("Print Char value: " + a);
		System.out.println("Print byte value: " + b);
		System.out.println("Print short value: " + c);
		System.out.println("Print int value: " + d);
		System.out.println("Print long value: " + e);
		System.out.println("Print float value: " + f);
		System.out.println("Print double value: " + g);
		System.out.println("Print string value: " + h);
		

	}

}