package week1;

import java.util.Scanner;

public class MobileBalance {

	public static void main(String[] args) {
		// For each and every action mobile balance should get reduce
		Scanner get = new Scanner(System.in);
		int Currbalance = 4;
		while(Currbalance>0)
		{
			System.out.print("Enter the action(Sms or Call) you want to perform: ");
			String action = get.next();

			switch(action)
			{
			case "Sms" : 
				if(Currbalance>=1) {
					System.out.println("Enter the number you want to send Sms: ");
					long mobilenumber = get.nextLong();
					MobilePhone.Sendsms(mobilenumber, "Hello");
					Currbalance = Currbalance - 1; 
					System.out.println("Your current balance is " + Currbalance);} 
				else { System.out.println("You don't have enough balance"); }
				break;

			case "Call" : 
				if(Currbalance>=2) {
					System.out.println("Enter the number you want to call: ");
					String Name = get.next();
					MobilePhone.CallPerson(Name); 
					Currbalance = Currbalance - 2; 
					System.out.println("Your current balance is " + Currbalance);} 
				else { System.out.println("You don't have enough balance");}
				break;

			default :
				System.out.println("Please enter valid option");
			}
		}
		get.close();
	}

}