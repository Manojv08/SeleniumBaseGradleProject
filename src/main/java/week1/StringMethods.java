package week1;

import java.util.Scanner;

public class StringMethods {

	public static void main(String[] args) {
		// To use all the String methods
		
		Scanner get = new Scanner(System.in);
		System.out.print("Enter the String: ");
		String a = get.nextLine();
		
		String e = "Sruthi Angelin";
		
		String x = "Manoj";
		
		System.out.println("length of the string is: " + a.length());
		
		char c[] = a.toCharArray();
		for(char d : c)
		{
			System.out.println("Char Array is printed here: "+ d);
		}
		
		System.out.println("Print a particular char in a string: "+ a.charAt(2));
		
		System.out.println("Convert to Lowercase: "+ a.toLowerCase());
		
		System.out.println("Convert to Uppercase: "+ a.toUpperCase());
		
		System.out.println("Check strings are equal or not: " + a.equals(e));
		
		System.out.println("Check Strings are equalIgnoreCase: " + a.equalsIgnoreCase(e));
		
		System.out.println("Print the substring: "+ a.substring(0, 3));
		
		System.out.println("Print the index value of a char specified: "+ a.indexOf('o'));
		
		System.out.println("Print the index value of string specified: "+ a.indexOf("noj"));
		
		System.out.println("Print the last instance of index value of a char given: "+ a.lastIndexOf('e'));
		
		System.out.println("Trimmed the given text: "+ a.trim());
		
		String[] f = a.split("u");
		
		for(String g : f)
		{
			System.out.println(g);
		}
		
		System.out.println("Replace a particular string: " + a.replace('i', 'e'));
		
		System.out.println("Concatenation: " + a.concat(x));
		
		System.out.println("Contains: " + a.contains("lin"));
		
		System.out.println("Input Starts with sr? " + a.startsWith("sr"));
		
		System.out.println("Input ends with lin? " + a.endsWith("lin"));
		
		get.close();
	}

}