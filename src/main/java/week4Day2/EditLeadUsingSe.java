package week4Day2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EditLeadUsingSe extends ProjectMethods {
	@BeforeClass(groups="common")
	public void setData() {
		testCaseName = "EditLeadUsingSe";
		testCaseDescription = "Edit a Lead";
		author = "Mano";
		category = "Regression";
		fileNamedesc = "editlead";
	}
//	@Test(dependsOnMethods = "week4Day2.CreateLeadUsingSe.crLead")
//	@Test(groups="sanity", dependsOnGroups="smoke")
	@Test(dataProvider = "fetchData")
	public void edLead(String fName, String cName) {
		WebElement elelead = locateElement("linktext", "Leads");
		click(elelead);
		WebElement eleflead = locateElement("linktext", "Find Leads");
		click(eleflead);
		WebElement elefname = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(elefname, fName);
		WebElement elefindlead = locateElement("xpath", "//button[text()='Find Leads']");
		click(elefindlead);
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")));
		
		WebElement firstresult = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		click(firstresult);
		
		verifyTitle("View Lead | opentaps CRM");
		
		WebElement eleedit = locateElement("linktext", "Edit");
		click(eleedit);
		
		WebElement coname = locateElement("id" , "updateLeadForm_companyName");
		coname.clear();
		type(coname, cName);
		
		WebElement update = locateElement("xpath" , "//input[@value='Update']");
		click(update);
		
		WebElement viewname = locateElement("id", "viewLead_companyName_sp");
		System.out.println("Confirmed the new updated company name: " + getText(viewname));
		
	}
}