package week4Day2;

import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class CreateLeadUsingSe extends ProjectMethods{
	@BeforeClass(groups="common")
	public void setData() {
		testCaseName = "CreateLeadUsingSe";
		testCaseDescription = "Create a New Lead";
		author = "Manoj";
		category = "Smoke";
		fileNamedesc = "createlead";
	}
//	@Test(invocationCount=2, invocationTimeOut=30000)
	@Test(groups="smoke" , dataProvider = "fetchData")
	public void crLead(String cName, String fname, String lName, String areaCd, String phNum) {
		WebElement elecrlead = locateElement("linktext", "Create Lead");
		click(elecrlead);
		WebElement eleconame = locateElement("id", "createLeadForm_companyName");
		type(eleconame, cName);
		WebElement elefname = locateElement("id", "createLeadForm_firstName");
		type(elefname, fname);
		WebElement elelname = locateElement("id", "createLeadForm_lastName");
		type(elelname, lName);
		WebElement elesrc = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(elesrc, "Employee");
		WebElement pharea = locateElement("id", "createLeadForm_primaryPhoneAreaCode");
		type(pharea, areaCd);
		WebElement phnum = locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(phnum, phNum);
		WebElement elecrbutton = locateElement("class" , "smallSubmit");
		click(elecrbutton);
	}
	
	
}