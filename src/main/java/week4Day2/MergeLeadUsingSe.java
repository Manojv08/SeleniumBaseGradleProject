package week4Day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class MergeLeadUsingSe extends ProjectMethods{
	@Test(enabled = false)
	public void mergeLed() throws InterruptedException {
		// To merge lead using SeMethods
//		login();
		WebElement elelead = locateElement("linktext", "Leads");
		click(elelead);
		WebElement findl = locateElement("linktext", "Merge Leads");
		click(findl);
		WebElement img1 = locateElement("xpath", "//input[@id='partyIdFrom']/following-sibling::a/img");
		click(img1);
		switchToWindow(1);
		WebElement loc1 = locateElement("xpath", "//input[@name='id']");
		type(loc1, "10430");
		WebElement floc1 = locateElement("xpath", "//button[text()='Find Leads']");
		click(floc1);

		WebElement slc1 = locateElement("linktext", "10430");
		clickWithNoSnap(slc1);
		switchToWindow(0);
		
		WebElement img2 = locateElement("xpath", "//input[@id='partyIdTo']/following-sibling::a/img");
		click(img2);
		switchToWindow(1);
		
		WebElement loc2 = locateElement("xpath", "//input[@name='id']");
		type(loc2, "10433");
		WebElement floc2 = locateElement("xpath", "//button[text()='Find Leads']");
		click(floc2);
		WebElement slc2 = locateElement("linktext", "10433");
		clickWithNoSnap(slc2);
		switchToWindow(0);
		
		takeSnap();
		WebElement merge = locateElement("linktext", "Merge");
		clickWithNoSnap(merge);
		acceptAlert();
		
		WebElement eleflead = locateElement("linktext", "Find Leads");
		click(eleflead);
		WebElement loc3 = locateElement("xpath", "//input[@name='id']");
		type(loc3, "10430");
		
		WebElement fndld = locateElement("xpath", "//button[text()='Find Leads']");
		click(fndld);
		
		WebElement errmsg = locateElement("class", "x-paging-info");
		String errtxt = getText(errmsg);
		System.out.println("Verified the Error message: " + errtxt);
		
	}
}