package week4Day2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DuplicateLeadUsingSe extends ProjectMethods{
	@BeforeClass
	public void setData() {
		testCaseName = "DuplicateLeadUsingSe";
		testCaseDescription = "Duplicate a Lead";
		author = "Mano";
		category = "Integration";
	}
	@Test
	public void dupLead() {
		WebElement elelead = locateElement("linktext", "Leads");
		click(elelead);
		WebElement eleflead = locateElement("linktext", "Find Leads");
		click(eleflead);
		WebElement email = locateElement("linktext", "Email");
		click(email);
		WebElement entemail = locateElement("name", "emailAddress");
		type(entemail, "vdotmanoj08@gmail.com");
		WebElement fndld = locateElement("xpath", "//button[text()='Find Leads']");
		click(fndld);
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-firstName'])[1]/a")));
		
		WebElement fleadnm = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-firstName'])[1]/a");
		String fname = getText(fleadnm);
		System.out.println("Captured the Name of first resulting Lead: " + fname);
		click(fleadnm);
		
		WebElement duplead = locateElement("linktext", "Duplicate Lead");
		click(duplead);
		
		verifyTitle("Duplicate Lead | opentaps CRM");
				
		WebElement crlead = locateElement("class" , "smallSubmit");
		click(crlead);
		
		WebElement viewfname = locateElement("id", "viewLead_firstName_sp");
		boolean namecheck = getText(viewfname).equals(fname);
		System.out.println("Confirm the duplicated lead name is same as captured name: " + namecheck);
		
	}

}