package week4Day2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import Framework.SeMethods;
import week6Day2.ReadExcelApachePOI;

public class ProjectMethods extends SeMethods{
	
	@DataProvider(name = "fetchData")
	public Object[][] getData() throws IOException {
		Object[][] excelData = ReadExcelApachePOI.getExcelData(fileNamedesc);
		return excelData;
	}
	
	@BeforeMethod(groups="common")
	@Parameters({"browser","url","username","password"})
	public void login(String browser, String url, String uname, String pswd) {
		startApp(browser, url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, uname);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, pswd);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement elecrm = locateElement("linktext", "CRM/SFA");
		click(elecrm);
	}
	
	@AfterMethod(groups="common")
	public void closewindow() {
		closeBrowser();
	}
}