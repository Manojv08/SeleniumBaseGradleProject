package week4Day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class MouseDroppable {

	public static void main(String[] args) {
		// To do mouse drag and drop
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://jqueryui.com/droppable/");
		driver.switchTo().frame(0);
		WebElement drag = driver.findElementByXPath("//div[@id='draggable']/p");
		WebElement drop = driver.findElementByXPath("//div[@id='droppable']");
		Actions builder = new Actions(driver);
		builder.dragAndDrop(drag, drop).perform();
	}

}