package week4Day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException, IOException {
		// To Merge Lead

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//input[@id='username']").sendKeys("DemoSalesManager");
		driver.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		driver.findElementByXPath("//a[contains(text(),'CRM/SFA')]").click();
		driver.findElementByXPath("//a[text()='Leads']").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("//input[@id='partyIdFrom']/following-sibling::a/img").click();
		Set<String> winset1 = driver.getWindowHandles();
		List<String> winlist1 = new ArrayList<String>();
		winlist1.addAll(winset1);
		driver.switchTo().window(winlist1.get(1));
		driver.findElementByXPath("//div[@id='x-form-el-ext-gen25']/input").sendKeys("10036");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")));

		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
		driver.switchTo().window(winlist1.get(0));
		driver.findElementByXPath("//input[@id='partyIdTo']/following-sibling::a/img").click();
		Set<String> winset2 = driver.getWindowHandles();
		List<String> winlist2 = new ArrayList<String>();
		winlist2.addAll(winset2);

		driver.switchTo().window(winlist2.get(1));
		driver.findElementByXPath("//div[@id='x-form-el-ext-gen25']/input").sendKeys("10037");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")));

		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
		driver.switchTo().window(winlist2.get(0));
		
		driver.findElementByLinkText("Merge").click();
		driver.switchTo().alert().accept();
		
		driver.findElementByLinkText("Find Leads").click();	
		driver.findElementByXPath("(//div[contains(@id,'x-form-el-ext-gen')])[18]/input").sendKeys("10036");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		Thread.sleep(3000);		
		String text = driver.findElementByClassName("x-paging-info").getText();
		System.out.println("Verified the Error message: " + text);
		
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./snaps/Mergelead.png");
		FileUtils.copyFile(src, dest);

	}

}