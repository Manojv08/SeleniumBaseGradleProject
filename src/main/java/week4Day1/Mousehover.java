package week4Day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Mousehover {

	public static void main(String[] args) {
		// To try mouse hover
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.flipkart.com/");
		driver.findElementByXPath("//button[text()='✕']").click();
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByXPath("//span[text()='Men']")).perform();
		driver.findElementByLinkText("T-Shirts").click();
		driver.findElementByLinkText("Metronaut Printed Men's Round Neck Dark Blue T-Shirt").click();
		
	}

}