package week3Day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CountLinks {

	public static void main(String[] args) {
		// To count links
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		List<WebElement> linknum = driver.findElementsByTagName("a");
		for (WebElement list : linknum) {
			System.out.println(list.getText());
		}
		System.out.println("No.of.links on the leaftap page: " + linknum.size());
		linknum.get(1).click();
	}

}