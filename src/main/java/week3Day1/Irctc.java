package week3Day1;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Irctc {

	public static void main(String[] args) throws InterruptedException {
		// To automate and enter details in IRCTC website
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		/*System.setProperty("webdriver.edge.driver", "./drivers/MicrosoftWebDriver.exe");
		EdgeDriver driver = new EdgeDriver();*/
		
		driver.manage().window().maximize();
		
		driver.get("https://www.irctc.co.in/");
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Sign up").click();
		driver.findElementById("userRegistrationForm:userName").sendKeys("Mano0805");
		driver.findElementById("userRegistrationForm:password").sendKeys("Angelin1611");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Angelin1611");
		
		Select obj = new Select(driver.findElementById("userRegistrationForm:securityQ"));
		obj.selectByValue("6");
		
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("YamahaFz");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Manoj");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("V");
		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		
		Select obj1 = new Select(driver.findElementById("userRegistrationForm:dobDay"));
		obj1.selectByValue("08");
		
		Select obj2 = new Select(driver.findElementById("userRegistrationForm:dobMonth"));
		obj2.selectByVisibleText("MAY");
		
		Select obj3 = new Select(driver.findElementById("userRegistrationForm:dateOfBirth"));
		obj3.selectByValue("1994");
		
		Select obj4 = new Select(driver.findElementById("userRegistrationForm:occupation"));
		obj4.selectByVisibleText("SelfEmployed");
		
		Select obj5 = new Select(driver.findElementById("userRegistrationForm:countries"));
		obj5.selectByValue("94");
		
		driver.findElementById("userRegistrationForm:email").sendKeys("vdotmanoj08@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9095023454");
		
		Select obj6 = new Select(driver.findElementById("userRegistrationForm:nationalityId"));
		obj6.selectByValue("94");
		
		driver.findElementById("userRegistrationForm:address").sendKeys("117/135");
		driver.findElementById("userRegistrationForm:area").sendKeys("Attur");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("636108" , Keys.TAB);
		
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(3000);
		
		Select obj7 = new Select(driver.findElementById("userRegistrationForm:cityName"));
		obj7.selectByValue("Salem");
		
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(3000);
		
		Select obj8 = new Select(driver.findElementById("userRegistrationForm:postofficeName"));
		obj8.selectByVisibleText("Narasingapuram S.O (Salem)");
		
		driver.findElementById("userRegistrationForm:landline").sendKeys("281604");
	}

}