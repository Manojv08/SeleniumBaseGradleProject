package week3Day1;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LoginLeaftapAndCreate {

	public static void main(String[] args) throws InterruptedException {
		// To login leaftap

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		/*System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
		FirefoxDriver driver = new FirefoxDriver();*/
		
		/*System.setProperty("webdriver.edge.driver", "./drivers/MicrosoftWebDriver.exe");
		EdgeDriver driver = new EdgeDriver();*/

		driver.get("http://leaftaps.com/opentaps/");
		
		driver.manage().window().maximize();
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();

		driver.findElementById("createLeadForm_companyName").sendKeys("Alight");
		driver.findElementById("createLeadForm_firstName").sendKeys("Manoj");
		driver.findElementById("createLeadForm_lastName").sendKeys("V");

		Select obj = new Select(driver.findElementById("createLeadForm_dataSourceId"));
		obj.selectByVisibleText("Conference");

		Select obj1 = new Select(driver.findElementById("createLeadForm_marketingCampaignId"));		
		obj1.selectByValue("CATRQ_CARNDRIVER");

		/* To print all the options in the drop down
		List<WebElement> obj1opt = obj1.getOptions();
		To select Last option in a drop down
		obj1.selectByIndex(obj1opt.size()-1);
		for (WebElement eachopt : obj1opt) {
			System.out.println(eachopt.getText());
		}*/
		
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Mano");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Venu");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Engineer");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Mr");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Electrical");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("500000");
		
		Select obj2 = new Select(driver.findElementById("createLeadForm_currencyUomId"));
		obj2.selectByValue("INR");
		
		Select obj3 = new Select(driver.findElementById("createLeadForm_industryEnumId"));
		obj3.selectByIndex(1);
		
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("333");
		
		Select obj4 = new Select(driver.findElementById("createLeadForm_ownershipEnumId"));
		obj4.selectByVisibleText("Corporation");
		
		driver.findElementById("createLeadForm_sicCode").sendKeys("1927");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("CIT");
		driver.findElementById("createLeadForm_description").sendKeys("I'm doing a selenium Automation");
		driver.findElementById("createLeadForm_importantNote").sendKeys("This is my first Automation program");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").clear();
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("+91");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("75");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("4805");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("vdotmanoj08@gmail.com");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9095023454");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Manoj");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("alightsolutions.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("Manoj");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("Mano");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("135/117");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Attur");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Salem");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("636108");
		
		Select obj5 = new Select(driver.findElementById("createLeadForm_generalCountryGeoId"));
		obj5.selectByValue("IND");
		
//		Thread.sleep(3000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		Select obj6 = new Select(driver.findElementById("createLeadForm_generalStateProvinceGeoId"));
		obj6.selectByValue("IN-TN");
		
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("4");
		
		driver.findElementByClassName("smallSubmit").click();
		
		/*To get and check the text which we have given as input
		String a = driver.findElementById("viewLead_firstName_sp").getText();
		System.out.println(a);*/
	}

}