package dailychallengeweek1;

import java.util.Scanner;

public class PalindromeString {

	public static void main(String[] args) {
		// To find given number is a palindrome or not

		String a,b = "";
		Scanner get = new Scanner(System.in);
		System.out.print("Enter the number: ");
		a = get.next();
		int c = a.length();

		for(int i=(c-1);i>=0;i--)
		{
			b=b+a.charAt(i);
		}
		
		if(a.equals(b))
		{
			System.out.println("Given input is palindrome");
		}
		else
		{
			System.out.println("Given input is not a palindrome");
		}
		get.close();
	}

}