package dailychallengeweek1;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class ArraySort {

	public static void main(String[] args) {
		// To Sort the given input array in ascending and descending order
		
		Scanner get = new Scanner(System.in);
		System.out.println("Enter the length of the array: ");
		int m = get.nextInt();
		Integer[] a = new Integer[m];
		
		System.out.println("Enter the input array values: ");		
		
		for (int i=0;i<m;i++)
		{
			a[i] = get.nextInt();
		}
		
		Arrays.sort(a);
		
		System.out.println("Ascending Order: " + Arrays.toString(a));
		
		Arrays.sort(a, Collections.reverseOrder());
		
		System.out.println("Descending Order: " + Arrays.toString(a));
		
		get.close();

	}

}