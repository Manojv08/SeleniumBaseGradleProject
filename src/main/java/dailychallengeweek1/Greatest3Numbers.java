package dailychallengeweek1;

import java.util.Scanner;

public class Greatest3Numbers {

	public static void main(String[] args) {
		// Greatest of three numbers

		Scanner get = new Scanner(System.in);
		System.out.print("Enter the First number : ");
		int a = get.nextInt();
		System.out.print("Enter the Second number : ");
		int b = get.nextInt();
		System.out.print("Enter the Third number : ");
		int c = get.nextInt();

		int d = c > (a>b?a:b)?c:(a>b?a:b);

		System.out.println("Greatest of three numbers is " + d);

		get.close();

	}

}