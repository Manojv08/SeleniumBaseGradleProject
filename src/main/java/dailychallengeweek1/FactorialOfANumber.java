package dailychallengeweek1;

import java.util.Scanner;

public class FactorialOfANumber {

	public static void main(String[] args) {
		// Factorial of a given number

		Scanner get = new Scanner(System.in);
		System.out.print("Enter the number to calculate its factorial: ");
		int a = get.nextInt();
		int b = 1;
		for(int i=1;i<=a;i++)
		{
			b = b*i;
		}
		get.close();

		System.out.println("The Factorial of a given number is " + b);
	}

}