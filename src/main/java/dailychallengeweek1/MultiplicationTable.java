package dailychallengeweek1;

import java.util.Scanner;

public class MultiplicationTable {

	public static void main(String[] args) {
		// Multiplication tables for a given number

		Scanner get = new Scanner(System.in);
		System.out.print("Enter the number for which you need to print the Multiplication table : ");
		int a = get.nextInt();

		System.out.print("Enter the number of instances you need to print : ");
		int b = get.nextInt();

		for(int i = 1; i <= b; i++)
		{
			System.out.println(i + " * " + a + " = " + i*a);
		}

		get.close();

	}

}