package dailychallengeweek1;

import java.util.Scanner;

public class ReversetheNumber {

	public static void main(String[] args) {
		// To reverse and print the given input number
		
		String a;
		Scanner get = new Scanner(System.in);
		System.out.print("Enter the number: ");
		a = get.next();
/*int c = a.length();

		for(int i=(c-1);i>=0;i--)
		{
			b=b+a.charAt(i);
		}*/
		
		StringBuilder b = new StringBuilder(a);
		b.reverse();
		
		System.out.println("The Reversed Number is: " + b);
		
		get.close();

	}

}