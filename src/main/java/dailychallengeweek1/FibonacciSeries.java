package dailychallengeweek1;

import java.util.Scanner;

public class FibonacciSeries {

	public static void main(String[] args) {
		// Fibonacci series of a given number

		Scanner get = new Scanner(System.in);
		System.out.print("Enter the number till you want to print in fibonacci series: ");
		int a = get.nextInt();
		int n1=0, n2=1;

		while(n1<=a)
		{
			System.out.print(n1 + " ");
			int sum = n1+n2;
			n1=n2;
			n2=sum;
		}
		get.close();
	}
}