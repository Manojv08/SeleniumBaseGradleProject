package dailychallengeweek1;

public class StringWordReverse {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String s = "Madam is a good madam";
		String lowerCaseds = s.toLowerCase();
		String w[] = lowerCaseds.split("\\s");
		for(int i=0;i<w.length;i++) {
		    char[] c = w[i].toCharArray();
		    int length =c.length;
		    String reverse="";
		    int z = 0;
		    for(int j=length-1;j>=0;j--)
		    {
		    	if(z==0) {
		    		reverse = reverse + Character.toUpperCase(c[j]);
		    	}else {
		    	reverse =reverse + c[j];
		    	}
		    	z++;
		    }
		    
		    System.out.print(reverse+" ");
		}
		
     
	}

}
