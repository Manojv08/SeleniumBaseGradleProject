package dailychallengeweek1;

import java.util.Scanner;

public class NoOfDaysInMonth {

	public static void main(String[] args) {
		// To find the number of days in a month

		int daysinmonth = 0;
		String monthname = null;

		Scanner get = new Scanner(System.in);
		System.out.print("Enter the month number: ");
		int month = get.nextInt();
		System.out.print("Enter the year: ");
		int year = get.nextInt();

		switch(month)
		{
		case 1 : monthname = "January";
		daysinmonth = 31;
		break;

		case 2 : monthname = "February";
		if(year%400==0 || ((year%100 != 0) && (year%4 ==0))) 
		{
			daysinmonth = 29;
		}
		else 
		{
			daysinmonth = 28;
		}
		break;
		case 3 : monthname = "March";
		daysinmonth = 31;
		break;
		case 4 : monthname = "April";
		daysinmonth = 30;
		break;
		case 5 : monthname = "May";
		daysinmonth = 31;
		break;
		case 6 : monthname = "June";
		daysinmonth = 30;
		break;
		case 7 : monthname = "July";
		daysinmonth = 31;
		break;
		case 8 : monthname = "August";
		daysinmonth = 31;
		break;
		case 9 : monthname = "September";
		daysinmonth = 30;
		break;
		case 10 : monthname = "October";
		daysinmonth = 31;
		break;
		case 11 : monthname = "November";
		daysinmonth = 30;
		break;
		case 12 : monthname = "December";
		daysinmonth = 31;
		break;
		}
		get.close();

		System.out.println(monthname+" "+ year + " has " + daysinmonth + " days");
	}

}