package dailychallengeweek4;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordCheckUsingRegEx {

	public static void main(String[] args) {
		/*To check password has the following criteria
		must have at least 10 characters
		only letters and digits
		must contain at least two digits and two letters
		must contain at least one capital letter*/
		
		Scanner get = new Scanner(System.in);
		System.out.print("Enter the String: ");
		String a = get.next();
		
		if(a.length()>=10) {
			Pattern p = Pattern.compile("^(?=.*[0-9]{2,})(?=.*[a-z]{1,})(?=.*[A-Z]{1,})(?=\\S+$)(?=[^@#$%&*]).{10,}$");
			Matcher m = p.matcher(a);
			System.out.println(m.matches());
		}else {
			System.err.println("Your password doesn't have a minimum of 10 characters");
		}
		
		get.close();

	}

}
