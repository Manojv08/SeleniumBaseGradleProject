package dailychallengeweek4;

import java.util.InputMismatchException;
import java.util.Scanner;

public class LargestButLessThanGivenNumber {

	public static void main(String[] args) {
		// To find the largest number but less than the given number and also it should not contain the given digit
		
		Scanner ip = new Scanner(System.in);
		int a = 0;
		try {
			System.out.print("Enter the Input Number: ");
			a = ip.nextInt();
		} catch (InputMismatchException e) {
			System.err.println("Please enter only numbers");
		}
		
		CharSequence b = null;
		try {
			System.out.print("Enter the digit: ");
			b = ip.next();
		} catch (Exception e) {
			System.err.println("Unknown Exception");
		}
		
		int c=a-1;
		
		while(c>0) {
//			we can use any one of the if condition - both checks the same
//			if(String.valueOf(c).contains(b))
			if(Integer.toString(c).contains(b))
			{
				c--;
			}else {
				System.out.println("Largest number but less than given number without input digit: " + c);
				break;
			}
		}
		ip.close();

	}

}