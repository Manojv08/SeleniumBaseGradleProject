package dailychallengeweek4;

import java.util.Scanner;

public class SumOfMultiplesOf3And5 {

	public static void main(String[] args) {
		// To print the Sum of all the Multiples of 3 & 5 upto the given number
		
		Scanner ip = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int a = ip.nextInt();
		int sum =0;
		System.out.print("Multiples of 3 & 5 until the number " + a + " are: ");
		for (int i = 1; i < a; i++) {
			
			if((i%3==0) || (i%5==0))
			{
				System.out.print(i + " ");
				sum = sum+i;
			}
		}
		System.out.print("\n");
		ip.close();
		System.out.println("Sum of all the multiples of 3 & 5 is: " + sum);
	}
}