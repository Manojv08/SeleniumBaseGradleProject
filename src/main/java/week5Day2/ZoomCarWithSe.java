package week5Day2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Framework.SeMethods;

public class ZoomCarWithSe extends SeMethods{
	@BeforeClass
	public void setData() {
		testCaseName = "ZoomCarProject";
		testCaseDescription = "To work with ZoomCar website";
		author = "Manoj";
		category = "smoke";
	}
	@Test
	public void projectZoom() {
		// To do project with ZoomCar
		String brandbook = null;
		
		startApp("chrome", "https://www.zoomcar.com/chennai/");
		WebElement strtjrny = locateElement("linktext", "Start your wonderful journey");
		click(strtjrny);
		WebElement location = locateElement("xpath", "//div[@class='component-popular-locations']/div[2]");
		click(location);
		WebElement nextbtn = locateElement("xpath", "//button[text()='Next']");
		click(nextbtn);
		WebElement dayslct = locateElement("xpath", "//div[text()='Mon']");
		click(dayslct);
		click(nextbtn);
		WebElement dayslcted = locateElement("xpath", "//div[@class='day picked ']");
		String date = getText(dayslcted);
		System.out.println("Selected date and day is: " + date);
		WebElement donebtn = locateElement("xpath", "//button[text()='Done']");
		click(donebtn);
		List<WebElement> carlist = locateElements("xpath", "//div[@class='car-listing']");
		int size = carlist.size();
		System.out.println("No.of.car list displayed: " + size);
		Map<String, Integer> cardetails = new LinkedHashMap<String, Integer>();
		for (int i = 1; i <= size; i++) {
			WebElement eachcarnmele = locateElement("xpath", "(//div[@class='details']/h3)["+i+"]");
			String carname = getText(eachcarnmele);
			WebElement eachcarprele = locateElement("xpath", "(//div[@class='price'])["+i+"]");
			String caramt = getText(eachcarprele);
			String rpcaramt = caramt.replaceAll("[₹ ]", "");
			int carat = Integer.parseInt(rpcaramt);
			cardetails.put(carname, carat);			
		}
		Collection<Integer> prices = cardetails.values();
		List<Integer> carprices = new ArrayList<Integer>();
		carprices.addAll(prices);
		Collections.sort(carprices, Collections.reverseOrder());
		Integer highest = carprices.get(0);
		System.out.println("Map of car details" + cardetails);
		System.out.println("Highest price in the list: " + highest);
		
		for (Entry<String, Integer> eachcar: cardetails.entrySet()) {
			if(eachcar.getValue().equals(highest))
			{
				brandbook = eachcar.getKey();
				System.out.println("Highest value of car brand: " + brandbook);
			}
		}
		
		WebElement booking = locateElement("xpath", "//h3[text()='"+brandbook+"']/following::button");
		click(booking);
	}
}