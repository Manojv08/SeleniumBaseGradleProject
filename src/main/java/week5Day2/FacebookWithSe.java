package week5Day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Framework.SeMethods;

public class FacebookWithSe extends SeMethods{
	
	@BeforeClass
	public void setData() {
		testCaseName = "FbProject";
		testCaseDescription = "To work with FB to get Like Count";
		author = "Manoj";
		category = "Integration";
	}

	@Test
	public void facebookTL() {
		//To do project with fb using SeMethods
		startApp("chrome", "https://www.facebook.com/");
		WebElement email = locateElement("id", "email");
		type(email, "user");
		WebElement pswd = locateElement("id", "pass");
		type(pswd, "test");
		WebElement login = locateElement("xpath", "//input[@value='Log In']");
		click(login);
		WebElement search = locateElement("xpath", "(//input[@placeholder='Search'])[1]");
		type(search, "TestLeaf");
		WebElement srchbtn = locateElement("xpath", "//button[@data-testid='facebar_search_button']");
		click(srchbtn);
		WebElement TLlnk = locateElement("linktext", "TestLeaf");
		verifyDisplayed(TLlnk);
		WebElement likebutton = locateElement("xpath", "//div[text()='TestLeaf']/following::button");
		if(likebutton.getText().equals("Like")) {
			likebutton.click();
			reportStep("Testleaf like button is clicked now", "pass");
		} else if(likebutton.getText().equals("Liked")) {
			reportStep("Testleaf like button is already clicked, Current button text: " + likebutton.getText() , "pass");
		}
		click(TLlnk);
		verifyTitle("TestLeaf");
		WebElement likect = locateElement("xpath", "(//img[@alt='Highlights info row image']/following::div)[2]");
		String cttext = getText(likect);
		String likectrplc = cttext.replaceAll("\\D", "");
		System.out.println("Like count: " + likectrplc);

	}
}