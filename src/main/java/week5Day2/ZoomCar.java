package week5Day2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class ZoomCar {
	@Test
	public void projectZoom() throws InterruptedException {
		// Project
		String brandbook = null;

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.zoomcar.com/chennai/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByLinkText("Start your wonderful journey").click();
		driver.findElementByXPath("//div[@class='component-popular-locations']/div[2]").click();
		driver.findElementByXPath("//button[text()='Next']").click();
		driver.findElementByXPath("//div[text()='Mon']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//button[text()='Next']").click();
		String date = driver.findElementByXPath("//div[@class='day picked ']").getText();
		System.out.println("Selected date and day is: " + date);
		driver.findElementByXPath("//button[text()='Done']").click();		
		List<WebElement> carlist = driver.findElementsByXPath("//div[@class='car-listing']");
		int size = carlist.size();
		System.out.println("No.of.car list displayed: " + size);
		Map<String, Integer> cardetails = new LinkedHashMap<String, Integer>();
		for (int i = 1; i <= size; i++) {
			String carname = driver.findElementByXPath("(//div[@class='details']/h3)["+i+"]").getText();
			String caramt = driver.findElementByXPath("(//div[@class='price'])["+i+"]").getText();
			String rpcaramt = caramt.replaceAll("[₹ ]", "");
			int carat = Integer.parseInt(rpcaramt);
			cardetails.put(carname, carat);
		}
		
		Collection<Integer> prices = cardetails.values();
		List<Integer> carprices = new ArrayList<Integer>();
		carprices.addAll(prices);
		Collections.sort(carprices, Collections.reverseOrder());
		Integer highest = carprices.get(0);
		System.out.println("Map of car details" + cardetails);
		System.out.println("Highest price in the list: " + highest);
		
		for (Entry<String, Integer> eachcar: cardetails.entrySet()) {
			if(eachcar.getValue().equals(highest))
			{
				brandbook = eachcar.getKey();
				System.out.println("Highest value of car brand: " + brandbook);
			}
		}
		
		driver.findElementByXPath("//h3[text()='"+brandbook+"']/following::button").click();
		
	}

}