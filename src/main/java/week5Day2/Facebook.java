package week5Day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Facebook {

	public static void main(String[] args) {
		// To do project with fb
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeOptions op = new ChromeOptions();
		op.addArguments("--disable-notifications");
		ChromeDriver driver= new ChromeDriver(op);
		driver.get("https://www.facebook.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("email").sendKeys("user");
		driver.findElementById("pass").sendKeys("test");
		driver.findElementByXPath("//input[@value='Log In']").click();
		driver.findElementByXPath("(//input[@placeholder='Search'])[1]").sendKeys("TestLeaf");
		driver.findElementByXPath("//button[@data-testid='facebar_search_button']").click();
		boolean displayed = driver.findElementByLinkText("TestLeaf").isDisplayed();
		System.out.println("TestLeaf link is displayed or not: "+ displayed);
		WebElement likebutton = driver.findElementByXPath("//div[text()='TestLeaf']/following::button");
		if(likebutton.getText().equals("Like")) {
			likebutton.click();
		}else if(likebutton.getText().equals("Liked")) {
			System.out.println("Testleaf is already " + likebutton.getText());
		}
		
		driver.findElementByLinkText("TestLeaf").click();
		String title = driver.getTitle();
		if(title.contains("TestLeaf")) {
			System.out.println("Title contains: " + title);
		}
		
		String likect = driver.findElementByXPath("(//img[@alt='Highlights info row image']/following::div)[2]").getText();
		String likectrplc = likect.replaceAll("\\D", "");
		System.out.println("Like count: " + likectrplc);

	}

}
