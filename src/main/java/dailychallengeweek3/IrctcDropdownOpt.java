package dailychallengeweek3;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcDropdownOpt {

	public static void main(String[] args) throws InterruptedException {
		// To automate and enter details in IRCTC website
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		/*System.setProperty("webdriver.edge.driver", "./drivers/MicrosoftWebDriver.exe");
		EdgeDriver driver = new EdgeDriver();*/
		
		driver.manage().window().maximize();
		
		driver.get("https://www.irctc.co.in/");
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Sign up").click();
		
		Select obj5 = new Select(driver.findElementById("userRegistrationForm:countries"));
		obj5.selectByValue("94");
		
		List<WebElement> allopt = obj5.getOptions();
		for (WebElement eachopt : allopt) {
			System.out.println(eachopt.getText());
		}
	}
}