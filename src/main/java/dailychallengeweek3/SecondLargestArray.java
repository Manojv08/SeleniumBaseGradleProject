package dailychallengeweek3;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class SecondLargestArray {

	public static void main(String[] args) {
		// To find out the Second Largest in the given input array

		Scanner get = new Scanner(System.in);
		System.out.println("Enter the length of the array: ");
		int m = get.nextInt();
		Integer[] a = new Integer[m];
		
		System.out.println("Enter the input array values: ");		
		
		for (int i=0;i<m;i++)
		{
			a[i] = get.nextInt();
		}
		
		Arrays.sort(a, Collections.reverseOrder());
		
		System.out.println("The Second largest number: " + a[1]);
		
		get.close();
	}

}