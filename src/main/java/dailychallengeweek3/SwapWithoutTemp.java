package dailychallengeweek3;

import java.util.Scanner;
public class SwapWithoutTemp {

	public static void main(String[] args) {
		// To swap the value of 2 variables without using temp variable
		
		Scanner get = new Scanner(System.in);
		System.out.println("Enter two numbers to swap: ");
		int m = get.nextInt();
		int n = get.nextInt();
		System.out.println("Before swappting: Variable 1 = " +m + " and Variable 2 = " +n);
		m=m+n;
		n=m-n;
		m=m-n;
		System.out.println("After swappting: Variable 1 = " +m + " and Variable 2 = " +n);
		get.close();
	}

}