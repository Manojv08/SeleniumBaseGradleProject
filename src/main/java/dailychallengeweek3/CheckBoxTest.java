package dailychallengeweek3;

import org.openqa.selenium.chrome.ChromeDriver;

public class CheckBoxTest {

	public static void main(String[] args) {
		// To play with check boxes

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/checkbox.html");
		driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[1]/input[1]").click();
		driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[1]/input[3]").click();
		driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[1]/input[4]").click();
		driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[1]/input[5]").click();

		boolean selected = driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[2]/input").isSelected();
		if(selected)
		{
			System.out.println("Selenium check box is checked");
		}else {
			System.out.println("Selenium check box is not checked");
		}

		boolean selected2 = driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[3]/input[1]").isSelected();
		if(selected2)
		{
			driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[3]/input[1]").click();
		}

		boolean selected3 = driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[3]/input[2]").isSelected();
		if(selected3)
		{
			driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[3]/input[2]").click();
		}

		boolean opt1 = driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[4]/input[1]").isSelected();
		if(!opt1)
		{
			driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[4]/input[1]").click();
		}
		boolean opt2 = driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[4]/input[2]").isSelected();
		if(!opt2)
		{
			driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[4]/input[2]").click();
		}
		boolean opt3 = driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[4]/input[3]").isSelected();
		if(!opt3)
		{
			driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[4]/input[3]").click();
		}
		boolean opt4 = driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[4]/input[4]").isSelected();
		if(!opt4)
		{
			driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[4]/input[4]").click();
		}
		boolean opt5 = driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[4]/input[5]").isSelected();
		if(!opt5)
		{
			driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[4]/input[5]").click();
		}
		boolean opt6 = driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[4]/input[6]").isSelected();
		if(!opt6)
		{
			driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[4]/input[6]").click();
		}
	}

}