package dailychallengeweek3;

import java.util.Scanner;

public class LeapYear {

	public static void main(String[] args) {
		// To find it is a leap year or not
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a year:");
		int year = sc.nextInt();
		if((year%400 == 0) || ((year%100 != 0) && (year%4 ==0))) {
			System.out.println(year + " is a Leap Year");
		}
		else {
			System.out.println(year + " is not a Leap year");
		}
		sc.close();

	}

}