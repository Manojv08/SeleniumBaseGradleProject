package dailychallengeweek3;

import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelectDropDown {

	public static void main(String[] args) {
		// To select drop down options by different methods
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/Dropdown.html");
		
		Select one = new Select(driver.findElementById("dropdown1"));
		one.selectByIndex(4);
		
		Select two = new Select(driver.findElementByName("dropdown2"));
		two.selectByVisibleText("Selenium");
		
		Select three = new Select(driver.findElementById("dropdown3"));
		three.selectByValue("2");
		
		Select four = new Select(driver.findElementByClassName("dropdown"));
		List<WebElement> allopt = four.getOptions();
		System.out.println("No.of.options in the drop down: " + allopt.size());
		for (WebElement eachopt : allopt) {
			System.out.println(eachopt.getText());
		}
		four.selectByValue("3");
		
		driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[5]/select").sendKeys("Selenium");
		
		Select five = new Select(driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[6]/select"));
		five.selectByVisibleText("UFT/QTP");
		five.selectByVisibleText("Selenium");
	}

}