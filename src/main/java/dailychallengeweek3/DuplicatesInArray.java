package dailychallengeweek3;

import java.util.LinkedHashSet;
import java.util.Set;

public class DuplicatesInArray {

	public static void main(String[] args) {
		// To find out duplicate values in an Array
		
		int a[] = {13, 65, 15, 67, 88, 65, 13, 99, 67, 13, 65, 87, 13};
		
		Set<Integer> org = new LinkedHashSet<Integer>();
		Set<Integer> dup = new LinkedHashSet<Integer>();
		
		for (int i = 0; i < a.length-1; i++) {
			if(org.add(a[i])==false) {
				dup.add(a[i]);
			}
		}
		
		/*for (int i = 0; i < a.length-1; i++)
        {
            for (int j = i+1; j < a.length; j++)
            {
                if ((a[i] == a[j]) && (i != j))
                {
                    dup.add(a[j]);
                }
            }
        }*/
		
		System.out.println("Duplicate numbers are: " + org);

	}

}