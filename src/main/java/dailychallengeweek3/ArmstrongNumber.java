package dailychallengeweek3;

public class ArmstrongNumber {

	public static void main(String[] args) {
		// To find out the Armstrong number from the given range

		int n, a, sum = 0;
		System.out.print("Armstrong numbers from 100 to 1000: ");
		for(int i = 100; i <= 1000; i++)
		{
			n = i;
			while(n > 0)
			{
				a = n % 10;
				sum = sum + (int) Math.pow(a, 3);
				n = n / 10;
			}
			if(sum == i)
			{
				System.out.print(i+" ");
			}
			sum = 0;
		}
	}

}