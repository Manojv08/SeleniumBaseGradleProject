package week6Day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcelApachePOI {

	public static Object[][] getExcelData(String fileName) throws IOException {
		// To Integrate Excel with Java by using Apache POI

		//locate Workbook
		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+fileName+".xlsx");
		//locate particular sheet which contains data
		XSSFSheet sheet = wbook.getSheetAt(0);
		//find out the last row till which data exists
		int lastRowNum = sheet.getLastRowNum();
		//find out the last column till which data exists - this can be done based upon the header row
		short lastColNum = sheet.getRow(0).getLastCellNum();
		//Use for iterator to print all the cell values from excel sheet
		Object[][] data = new Object[lastRowNum][lastColNum];
		for (int j = 1; j <= lastRowNum; j++) {
			XSSFRow row = sheet.getRow(j);
			for (int i = 0; i < lastColNum; i++) {
				XSSFCell cell = row.getCell(i);
				String stringCellValue = cell.getStringCellValue();
				data[j-1][i] = stringCellValue;
				System.out.println(stringCellValue);
			} 
		}
		wbook.close();
		return data;
	}
}