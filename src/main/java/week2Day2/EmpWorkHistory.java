package week2Day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class EmpWorkHistory {

	public static void main(String[] args) {
		// To find out in which company the employee maximum worked
		
//		String[] a = {"tcs" , "cts" , "amazon" , "cts" , "wipro" , "cts" , "tcs" , "infosys"};
		
		Scanner in = new Scanner(System.in);
		System.out.print("Enter the number of companies you worked before: ");
		int a = in.nextInt();
		String s;
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < a; i++) {
			s = in.next();
			list.add(s);
		}
		
		int sz = list.size();
		Map<String, Integer> map = new LinkedHashMap<String, Integer>();
		for (int i = 0; i < sz; i++) {
			String x = list.get(i);
			if(map.containsKey(x))
			{
				map.put(x, map.get(x)+1);
			}else {
				map.put(x, 1);
			}
			
		}
		
		System.out.println(map);
		
		int maxvl = Collections.max(map.values());
		
		for (Entry<String, Integer> y : map.entrySet()) {
			if(y.getValue()==maxvl)
			{
				System.out.println(y.getKey());
			}
		}
		
		in.close();
		
	}

}