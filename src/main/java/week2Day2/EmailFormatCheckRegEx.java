package week2Day2;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailFormatCheckRegEx {

	public static void main(String[] args) {
		// To check Email format using RegEx
		
		Scanner get = new Scanner(System.in);
		System.out.print("Enter the String: ");
		String a = get.next();
		
		Pattern p = Pattern.compile(".+[@][a-z]+[.][a-z]+");
		
		Matcher m = p.matcher(a);
		
		System.out.println(m.matches());
		
		get.close();

	}

}