package week2Day2;

import java.util.Scanner;

public class ArrayException {

	public static void main(String[] args) {
		// To practice Exception Handling
		
		int[] a = {5 , 6 , 7 , 8 , 9};
		Scanner get = new Scanner(System.in);
		System.out.print("Enter the index value: ");
		int b = get.nextInt();
		try {
			System.out.println(a[b]);
		} catch (ArrayIndexOutOfBoundsException e) {
System.out.println("Invalid index number");
		}
		get.close();
		
	}
	
}