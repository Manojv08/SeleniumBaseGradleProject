package week2Day2;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
//import java.util.Scanner;

public class CountEachChar {

	public static void main(String[] args) {
		// To count each instance of char in a given string input

		String a = "amazon development center";
		String a1 = a.replaceAll(" ", "");
//		System.out.println(a1);
		/*Scanner get = new Scanner(System.in);
		System.out.print("Enter the String: ");
		String a = get.next();*/
		int b = a1.length();
		Map<Character, Integer> c = new LinkedHashMap<Character, Integer>();

		for (int i = 0; i < b; i++) 
		{
			char d = a1.charAt(i);
			if (c.containsKey(d)) {
				c.put(d, c.get(d)+1);
			}else
			{
				c.put(d, 1);
			}

		}
		for (Entry<Character, Integer> e : c.entrySet()) {
			System.out.println(e);
		}
		System.out.println(c);
//		get.close();
		
	}

}