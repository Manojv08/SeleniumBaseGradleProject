package week2Day2;

public class RemoveSpacesRegEx {

	public static void main(String[] args) {
		// To print given string using Regex manipulatioon
		
		String a = "Hi     Hello        World      .";
		
		String op = a.replaceAll("\\s+" , " ");
		
		System.out.println(op);
	}

}