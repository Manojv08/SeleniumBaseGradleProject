package week2Day2;

import java.util.Scanner;

public class RemoveAlphabetsRegEx {

	public static void main(String[] args) {
		// To remove/replace all alphabets in a mixed string of numbers and letters
		
		Scanner get = new Scanner(System.in);
		System.out.print("Enter the String: ");
		String a = get.next();
		String b = a.replaceAll("[a-zA-Z]", "");
		System.out.println(b);

		get.close();
	}

}