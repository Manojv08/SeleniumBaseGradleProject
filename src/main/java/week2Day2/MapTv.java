package week2Day2;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MapTv {

	public static void main(String[] args) {
		// To find number of Tv's in the Tv shop

		Integer c = 0;
		Map<String, Integer> obj = new LinkedHashMap<String , Integer>();
		obj.put("Sony", 3);
		obj.put("Onida", 2);
		obj.put("LG", 5);
		obj.put("BPL", 8);

		for (Entry<String, Integer> b : obj.entrySet()) {
			c = c + b.getValue();
		}
		System.out.println("Total number of values: " + c);

		Set<String> a = obj.keySet();
		System.out.println("KeySet: " + a);
		List<String> e = new ArrayList<String>();
		e.addAll(a);
		System.out.println("Added the set to List: " + e);
		System.out.println("Last key found: " + e.get(e.size()-1));
		System.out.println("Last value of last key found: " + obj.get(e.get(e.size()-1)));
		
		obj.put(e.get(0), obj.get(e.get(0))-1);
		
		System.out.println("one count reduced from First model: " + obj);

	}


}