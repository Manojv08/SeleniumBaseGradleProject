package week2Day2;

import java.util.Scanner;

public class ArrayExceptionMethod {

	public static void main(String[] args) {
		// To call exceptions from method using throw

		Scanner get = new Scanner(System.in);
		System.out.print("Enter the index value: ");
		int b = get.nextInt();

		ArrayExceptionMethod obj = new ArrayExceptionMethod();
		try {
			obj.test(b);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Invalid Index number");
		}
		get.close();
	}

	public void test(int c){
		int[] a = {5 , 6 , 7 , 8 , 9};
		if(c>=a.length)
		{
			throw new ArrayIndexOutOfBoundsException();
		}else
		{
			System.out.println("Your Array value is: " + a[c]);
		}
	}
}
