package week2Day2;

public class RegExManipulation {

	public static void main(String[] args) {
		// To play with a given string
		
		String a = "xsxrxuxtxhxix";
		int c = 0;
				
		for (int i = 0; i < a.length(); i++) {
			if(a.charAt(i)=='x')
			{
				c++;
			}
		}
		System.out.println(c);
		String b = a.replaceAll("[x]*", "");
		for (int i = 1; i <= c; i++) {
			b = b.concat("x");
		}
		
		System.out.println(b);
		
		
	}

}
