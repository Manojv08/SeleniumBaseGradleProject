package Framework;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import week5Day1.LearnReport;

public class SeMethods extends LearnReport implements WdMethods{
	public int i = 1;
	public static RemoteWebDriver driver;
	public static ChromeOptions op;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				op = new ChromeOptions();
				op.addArguments("--disable-notifications");
				driver = new ChromeDriver(op);
			} else if (browser.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportStep("The Browser "+browser+" Launched Successfully", "pass");
			//			System.out.println("The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			reportStep("The Browser "+browser+" not Launched", "fail");
			//			System.err.println("The Browser "+browser+" not Launched");
		} finally {
			takeSnap();
		}

	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id"	 : return driver.findElementById(locValue);
			case "class" : return driver.findElementByClassName(locValue);
			case "xpath" : return driver.findElementByXPath(locValue);
			case "linktext" : return driver.findElementByLinkText(locValue);
			case "name" : return driver.findElementByName(locValue);
			}
		} catch (NoSuchElementException e) {
			reportStep("The Element is not found", "fail");
			//			System.err.println("The Element is not found");
		} catch (Exception e) {
			reportStep("Unknow Exception ", "fail");
			//			System.err.println("Unknow Exception ");
		}
		return null;
	}

	public List<WebElement> locateElements(String locator, String locValue) {
		try {
			switch(locator) {
			case "id"	 : return driver.findElementsById(locValue);
			case "class" : return driver.findElementsByClassName(locValue);
			case "xpath" : return driver.findElementsByXPath(locValue);
			case "linktext" : return driver.findElementsByLinkText(locValue);
			case "name" : return driver.findElementsByName(locValue);
			}
		} catch (NoSuchElementException e) {
			reportStep("The Elements are not found", "fail");
			//			System.err.println("The Element is not found");
		} catch (Exception e) {
			reportStep("Unknow Exception ", "fail");
			//			System.err.println("Unknow Exception ");
		}
		return null;
	}
	
	public WebElement locateElement(String locValue) {
		try {
			return driver.findElementById(locValue);
		} catch (NoSuchElementException e) {
			reportStep("The Element is not found", "fail");
			//			System.err.println("The Element is not found");
		} catch (Exception e) {
			reportStep("Unknow Exception ", "fail");
			//			System.err.println("Unknow Exception ");
		}
		return null;
	}


	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			reportStep("The data "+data+" is Entered Successfully", "pass");
			//			System.out.println("The data "+data+" is Entered Successfully");
		} catch (WebDriverException e) {
			reportStep("The data "+data+" is Not Entered", "fail");
			//			System.err.println("The data "+data+" is Not Entered");
		} finally {
			takeSnap();
		}
	}

	public void clickWithNoSnap(WebElement ele) {
		try {
			ele.click();
			reportStep("The Element "+ele+" Clicked Successfully", "pass");
			//			System.out.println("The Element "+ele+" Clicked Successfully");
		} catch (Exception e) {
			reportStep("The Element "+ele+" is not Clicked", "fail");
			//			System.err.println("The Element "+ele+" is not Clicked");
		}
	}


	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			reportStep("The Element "+ele+" Clicked Successfully", "pass");
			//			System.out.println("The Element "+ele+" Clicked Successfully");
		} catch (WebDriverException e) {
			reportStep("The Element "+ele+" is not Clicked", "fail");
			//			System.err.println("The Element "+ele+" is not Clicked");
		} finally {
			takeSnap();
		}
	}

	@Override
	public String getText(WebElement ele) {
		try {
			String text = ele.getText();
			return text;
		} catch (NoSuchElementException e) {
			reportStep("Element" + ele + " not found", "fail");
			//			System.err.println("Element" + ele + " not found");
		} catch(Exception e) {
			reportStep("Unknown Exception found", "fail");
			//			System.err.println("Unknown Exception found");
		}
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			reportStep("The DropDown is Selected with VisibleText: "+value, "pass");
			//			System.out.println("The DropDown is Selected with VisibleText "+value);
		} catch (Exception e) {
			reportStep("The DropDown is not Selected with VisibleText: "+value, "fail");
			//			System.err.println("The DropDown is not Selected with VisibleText: "+value);
		} finally {
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select dd = new Select(ele);
			dd.selectByIndex(index);
			reportStep("The DropDown is Selected with index "+ index, "pass");
			//			System.out.println("The DropDown is Selected with index "+ index);
		} catch (Exception e) {
			reportStep("The DropDown is not Selected with index "+ index, "fail");
			//			System.err.println("The DropDown is not Selected with index "+ index);
		} finally {
			takeSnap();
		}
	}

	@Override
	public void verifyTitle(String expectedTitle) {
		String title = driver.getTitle();
		if(title.equals(expectedTitle))
		{
			reportStep("Current Page Title: "+title+" And expected title: "+expectedTitle+" matches exactly", "pass");
		}else {
			reportStep("Current Page Title: "+title+" And expected title: "+expectedTitle+ " doesn't matches", "fail");
		}
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		try {
			String text = ele.getText();
			boolean check = text.equals(expectedText);
			return check;
		} catch (NoSuchElementException e) {
			reportStep("Element" + ele + " not found", "fail");
			//			System.err.println("Element" + ele + " not found");
		} catch(Exception e) {
			reportStep("Unknown Exception found", "fail");
			//			System.err.println("Unknown Exception found");
		}
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		try {
			String text = ele.getText();
			boolean containscheck = expectedText.contains(text);
			return containscheck;
		} catch (NoSuchElementException e) {
			reportStep("Element" + ele + " not found", "fail");
			//			System.err.println("Element" + ele + " not found");
		} catch(Exception e) {
			reportStep("Unknown Exception found", "fail");
			//			System.err.println("Unknown Exception found");
		}
		return false;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		try {
			String attributevalue = ele.getAttribute(attribute);
			boolean equalscheck = attributevalue.equals(value);
			return equalscheck;
		} catch (NoSuchElementException e) {
			reportStep("Element" + ele + " not found", "fail");
			//			System.err.println("Element" + ele + " not found");
		} catch(Exception e) {
			reportStep("Unknown Exception found", "fail");
			//			System.err.println("Unknown Exception found");
		}
		return false;
	}

	@Override
	public boolean verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try {
			String attributevalue = ele.getAttribute(attribute);
			boolean containscheck = attributevalue.contains(value);
			return containscheck;
		} catch (NoSuchElementException e) {
			reportStep("Element" + ele + " not found", "fail");
			//			System.err.println("Element" + ele + " not found");
		} catch(Exception e) {
			reportStep("Unknown Exception found", "fail");
			//			System.err.println("Unknown Exception found");
		}
		return false;
	}

	@Override
	public void verifySelected(WebElement ele) {
		try {
			boolean selected = ele.isSelected();
			if(selected)
			{
				reportStep("Element " + ele + " is selected", "pass");
				//				System.out.println("Element " + ele + " is selected");
			}else {
				reportStep("Element " + ele + " is not selected", "pass");
				//				System.out.println("Element" + ele + " is not selected");
			}
		} catch (NoSuchElementException e) {
			reportStep("Element not found", "fail");
			//			System.err.println("Element not found");
		} catch(Exception e) {
			reportStep("Unknown Exception found", "fail");
			//			System.err.println("Unknown Exception found");
		}
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		try {
			boolean displayed = ele.isDisplayed();
			if(displayed)
			{
				reportStep("Element " + ele + " is displayed", "pass");
				//				System.out.println("Element " + ele + " is displayed");
			}else {
				reportStep("Element " + ele + " is not displayed", "pass");
				//				System.out.println("Element" + ele + " is not displayed");
			}
		} catch (NoSuchElementException e) {
			reportStep("Element not found", "fail");
			//			System.err.println("Element not found");
		} catch(Exception e) {
			reportStep("Unknown Exception found", "fail");
			//			System.err.println("Unknown Exception found");
		}
	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> winset = driver.getWindowHandles();
			List<String> winlist = new ArrayList<String>();
			winlist.addAll(winset);
			driver.switchTo().window(winlist.get(index));
		} catch (NoSuchWindowException e) {
			reportStep("Window which you have requested is not found", "fail");
			//			System.err.println("Window which you have requested is not found");
		} catch(Exception e) {
			reportStep("Unknown Exception found", "fail");
			//			System.err.println("Unknown Exception found");
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
		} catch (NoSuchFrameException e) {
			reportStep("Frame not found on the page", "fail");
			//			System.err.println("Frame not found on the page");
		} catch(Exception e) {
			reportStep("Unknown Exception found", "fail");
			//			System.err.println("Unknown Exception found");
		}
	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
		} catch (NoAlertPresentException e) {
			reportStep("Alert not found on the page", "fail");
			//			System.err.println("Alert not found on the page");
		} catch(UnhandledAlertException e) {
			reportStep("Alert is not handled properly", "fail");
			//			System.err.println("Alert is not handled properly");
		} catch(Exception e) {
			reportStep("Unknown Exception found", "fail");
			//			System.err.println("Unknown Exception found");
		}
	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
		} catch (NoAlertPresentException e) {
			reportStep("Alert not found on the page", "fail");
			//			System.err.println("Alert not found on the page");
		} catch(UnhandledAlertException e) {
			reportStep("Alert is not handled properly", "fail");
			//			System.err.println("Alert is not handled properly");
		} catch(Exception e) {
			reportStep("Unknown Exception found", "fail");
			//			System.err.println("Unknown Exception found");
		}
	}

	@Override
	public String getAlertText() {
		try {
			return driver.switchTo().alert().getText();
		} catch (NoAlertPresentException e) {
			reportStep("Alert not found on the page", "fail");
			//			System.err.println("Alert not found on the page");
		} catch(Exception e) {
			reportStep("Unknown Exception found", "fail");
			//			System.err.println("Unknown Exception found");
		}
		return null;
	}

	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			reportStep("IOException", "fail");
			//			System.err.println("IOException");
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();
	}
}